const gulp = require("gulp");
const babel = require("gulp-babel");
const sass = require("gulp-sass");
const minifyCSS = require("gulp-csso");
const concat = require("gulp-concat");
const sourcemaps = require("gulp-sourcemaps");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const minifyJS = require("gulp-uglify");

function css() {
  var plugins = [autoprefixer()];
  return gulp
    .src(["./sass/main.scss"])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss(plugins))
    .pipe(minifyCSS())
    .pipe(sourcemaps.write("../maps"))
    .pipe(gulp.dest("./dist/css/"));
}

function js() {
  return gulp
    .src(["./js/**/*.js"])
    .pipe(sourcemaps.init())
    .pipe(
      babel({
        presets: [["@babel/env", { modules: false }]]
      })
    )
    .pipe(concat("app.min.js"))
    .pipe(minifyJS())
    .pipe(sourcemaps.write("../maps"))
    .pipe(gulp.dest("./dist/js/"));
}

exports.default = function() {
  gulp.watch("sass/**/*.scss", css);
  gulp.watch("js/**/*.js", js);
};
